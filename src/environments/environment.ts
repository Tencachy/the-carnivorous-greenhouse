// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  mapBoxToken: 'pk.eyJ1IjoiamF2aWVycm9tZXJvaXRiIiwiYSI6ImNreWRjNWJubTB4cDkydW4wdGUyejIwa3gifQ.clFyHQ7X_uSH_ojh8yMVvg',
  firebaseConfig: {
    apiKey: "AIzaSyCnZqo51ee9OgmiuQQRQQOgHaWxt-ocoyY",
    authDomain: "the-carnivorous-greenhouse.firebaseapp.com",
    projectId: "the-carnivorous-greenhouse",
    storageBucket: "the-carnivorous-greenhouse.appspot.com",
    messagingSenderId: "189574640622",
    appId: "1:189574640622:web:832b29343c72be9594d4ff"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
