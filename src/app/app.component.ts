import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from './services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  isAdmin: boolean;

  constructor(private router: Router, private authService: AuthenticationService) {}

  logout() {
    this.authService.signOut();
  }

  checkAdmin() {
    if (localStorage.getItem('admin') === 'true'){
      this.isAdmin = true
    } else {
      this.isAdmin = false;
    }
  }

  openSideMenu() {
    this.checkAdmin();
  }

}
