import { Injectable } from '@angular/core';
import { CanLoad, Route, Router, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClientGuard implements CanLoad {

  constructor(private router: Router) {}

  async canLoad(): Promise<boolean> {
    if(localStorage.getItem('admin') !== 'true') {
      return true;
    }else {
      this.router.navigateByUrl('/admin/home', {replaceUrl : true});
      return true;
    }
  }
}
