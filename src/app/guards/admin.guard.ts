import { Injectable } from '@angular/core';
import { CanLoad, Route, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanLoad {
  
  constructor(private router: Router) {}

  async canLoad(): Promise<boolean> {
    if(localStorage.getItem('admin') === 'true') {
      return true;
    }else {
      this.router.navigateByUrl('/client/home', {replaceUrl : true});
      return true;
    }
  }
}
