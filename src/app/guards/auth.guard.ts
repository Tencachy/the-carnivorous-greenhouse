import { Injectable } from '@angular/core';
import { CanLoad, Router} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad {

  constructor(private router: Router) {}

  async canLoad(): Promise<boolean> {
    if(localStorage.getItem('authenticated') === 'true') {
      return true;
    }else {
      this.router.navigateByUrl('/login', {replaceUrl : true});
      return true;
    }
  }

}
