import { Injectable } from '@angular/core';
import { CanLoad, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanLoad {
  
  constructor(private router: Router) {}

  async canLoad(): Promise<boolean> {
    if(localStorage.getItem('authenticated') === 'true' && localStorage.getItem('admin') === 'true') {
      this.router.navigateByUrl('/admin/home', {replaceUrl : true});
      return true;
    }else if (localStorage.getItem('authenticated') === 'true' && localStorage.getItem('admin') !== 'true') {
      this.router.navigateByUrl('/client/home', {replaceUrl : true});
      return true;
    }else {
      return true;
    }
  }
  
}
