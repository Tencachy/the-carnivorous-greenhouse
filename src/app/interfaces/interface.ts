export interface Product {
    name: string;
    price: number;
    category: string;
}

export interface User {
    uid: string;
    email: string;
    displayName: string; 
    address: string;
    admin: boolean;
}

export interface Address {
    type: string;
    query: string[];
    features: Feature[];
    attribution: string;
}

export interface Properties {
    accuracy: string;
}

export interface Geometry {
    type: string;
    coordinates: number[];
    interpolated?: boolean;
    omitted?: boolean;
}

export interface Context {
    id: string;
    wikidata: string;
    text: string;
    short_code: string;
}

export interface Feature {
    id: string;
    type: string;
    place_type: string[];
    relevance: number;
    properties: Properties;
    text: string;
    place_name: string;
    center: number[];
    geometry: Geometry;
    address: string;
    context: Context[];
}
 