import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm: FormGroup
  user: any

  constructor(private router: Router, private authService: AuthenticationService, public formBuilder: FormBuilder) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: [''],
      password: ['']
    })
  }

  onSubmit() {
    if (!this.loginForm.valid) {
      return false;
    } else {
      this.logIn( 
        this.loginForm.value.email, 
        this.loginForm.value.password)
    }
  }

  logIn(email, password) {
    this.authService.signIn(email, password)
      .then((res) => {
        this.authService.getUser(email).subscribe(
          res=>{
            localStorage.setItem('admin', res['admin'].toString())
            localStorage.setItem('authenticated', 'true');
            if (res['admin']) {
              this.router.navigateByUrl('/admin/home');
            } else {
              this.router.navigateByUrl('/client/home');
            }
          }
        )
      }).catch((error) => {
        window.alert(error.message)
      })
  }
 

}
