import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.page.html',
  styleUrls: ['./product-create.page.scss'],
})
export class ProductCreatePage implements OnInit {

  createProductForm: FormGroup;

  constructor(private dataService: DataService,
    public formBuilder: FormBuilder,
    private router: Router) { }

  ngOnInit() {
    this.createProductForm = this.formBuilder.group({
      category: [''],
      name: [''],
      price: []
    })
  }

  onSubmit() {
    if (!this.createProductForm.valid) {
      return false;
    } else {
      this.dataService.createProduct(this.createProductForm.value)
      .then(() => {
        this.createProductForm.reset();
        this.router.navigate(['admin/product-list']);
      }).catch((err) => {
        console.log(err)
      });
    }
  }

}
