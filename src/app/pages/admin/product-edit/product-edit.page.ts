import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.page.html',
  styleUrls: ['./product-edit.page.scss'],
})
export class ProductEditPage implements OnInit {

  editProductForm: FormGroup;
  id: String;
  
  constructor(private dataService: DataService, private activatedRoute: ActivatedRoute,
    private router: Router, public formBuilder: FormBuilder) {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.dataService.getProduct(this.id).subscribe(
      res => {
        this.editProductForm = this.formBuilder.group({
          category: [res['category']],
          name: [res['name']],
          price: [res['price']]
        })
      });
  }

  ngOnInit(): void {
    this.editProductForm = this.formBuilder.group({
      category: [''],
      name: [''],
      price: []
    }) 
  }

  onSubmit() {
    this.dataService.updateBook(this.id, this.editProductForm.value);
  } 
}
