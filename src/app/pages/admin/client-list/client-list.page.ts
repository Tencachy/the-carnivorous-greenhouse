import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { User } from 'src/app/interfaces/interface';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.page.html',
  styleUrls: ['./client-list.page.scss'],
})
export class ClientListPage implements OnInit {

  users = [];
  clients = [];

  constructor(private data: DataService, private router: Router) { }

  ngOnInit() {
    this.getClients();
  }

  async getClients() {
    this.data.getClients().subscribe(
      res=>{
        this.users = res.map((item) => {
          return {
            id: item.payload.doc.id, ... item.payload.doc.data() as User
          }
        })

        this.clients = [];
        this.users.forEach(user => {
          if (!user.admin) {
            this.clients.push(user);
          }
        });
      }
    )
  }

  sendClientLocation(clientId) {
    let navigationExtras: NavigationExtras = {
      state: {
        clientId: clientId,
      }
    };
    this.router.navigate(['admin/map'], navigationExtras)
  }

}
