import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/interfaces/interface';
import { DataService } from 'src/app/services/data.service';
import { environment } from 'src/environments/environment';

declare var mapboxgl: any;

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {

  id: any;
  client: any;
  lat: any;
  lon: any;

  constructor(private dataService: DataService, private activatedRoute: ActivatedRoute,
    private router: Router) {
    this.activatedRoute.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.id = this.router.getCurrentNavigation().extras.state.clientId;
        dataService.getClient(this.id).subscribe(
          res => {
            this.client = res;
            if (!this.client.address) {
              console.log('no location')
            }
            dataService.getCoordinates(this.client.address).subscribe(
              res => {
                this.lon = res.features[0].center[0]
                this.lat = res.features[0].center[1]
                this.showMap()
              }
            )
          }
        );
      }
    });
  }

  ngOnInit() {
  }

  showMap() {
    mapboxgl.accessToken = environment.mapBoxToken
    var map = new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [this.lon, this.lat],
      zoom: 10
    });

    map.on('load', () => {
      map.resize();
    });

    map.addControl(new mapboxgl.NavigationControl());

    var marker = new mapboxgl.Marker({})
      .setLngLat([this.lon, this.lat])
      .setPopup(new mapboxgl.Popup().setHTML("<h4>"+ this.client.displayName +"</h4>"))
      .addTo(map);
  }

}
