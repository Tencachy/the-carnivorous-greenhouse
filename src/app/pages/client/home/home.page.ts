import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Product } from 'src/app/interfaces/interface';
import { DataService } from 'src/app/services/data.service';
import { NavigationExtras, Router } from '@angular/router';
import { EmailComposer } from '@awesome-cordova-plugins/email-composer/ngx';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  products = [];
  selectedProducts = [];

  constructor(private data: DataService, private alertController: AlertController, private router: Router, private emailComposer: EmailComposer) { }

  ngOnInit() {
    this.getProducts();
  }

  ionViewWillEnter() {
    this.products.forEach(product => {
      document.getElementById('quantity' + product.id).innerText = '0';
    });
  }

  getProducts() {
    this.data.getProducts().subscribe(
      res => {
        this.products = res.map((item) => {
          return {
            id: item.payload.doc.id, ...item.payload.doc.data() as Product
          }
        })
      }
    )
  }

  increaseProduct(productId) {
    let quantity: any = document.getElementById('quantity' + productId).innerText;
    let result = Number(quantity) + 1
    document.getElementById('quantity' + productId).innerText = result + ''
  }

  decreaseProduct(productId) {
    let quantity: any = document.getElementById('quantity' + productId).innerText;
    let result = quantity - 1
    if (result <= 0) {
      result = 0
    }
    document.getElementById('quantity' + productId).innerText = result + ''
  }

  checkout() {
    this.selectedProducts = []
    this.products.forEach(product => {
      let quantity = document.getElementById('quantity' + product.id).innerText;
      let relation = []
      if (quantity != '0') {
        relation.push(product.name)
        relation.push(quantity)
        relation.push(product.price)
        this.selectedProducts.push(relation)
      }
    });

    let checkoutMessage = ''
    let total = 0;
    this.selectedProducts.forEach(product => {
      checkoutMessage = checkoutMessage + '<div class="product"><p>' + product[0] + '</p><p>x' + product[1] + '</p></div>'
      total = total + (Number(product[1]) * product[2])
    });
    checkoutMessage = checkoutMessage + '<h3>Total: ' + (Math.round(total * 100) / 100).toFixed(2) + '€</h3>'

    let totalPrice = (Math.round(total * 100) / 100).toFixed(2)

    this.confirmCheckout(checkoutMessage, totalPrice);
  }

  async confirmCheckout(checkoutMessage, total) {
    const alert = await this.alertController.create({
      cssClass: 'custom-alert',
      header: 'Cesta',
      message: checkoutMessage,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          id: 'cancel-button',
          handler: (blah) => {
          }
        }, {
          text: 'Confirmar',
          id: 'confirm-button',
          handler: () => {
            this.sendEmail();
            this.sendCheckoutData(total)
          }
        }
      ]
    });

    await alert.present();
  }

  sendEmail() {
    let codedUser = localStorage.getItem('user')
    let user = JSON.parse(codedUser);
    let email = {
      to: user.email,
      attachments: ['file://img/TheCarnivorousGreenhouse.png'],
      subject: 'Checkout information',
      body: 'Thank you for buying at The Carnivorous Greenhouse', isHtml: true
    }
    this.emailComposer.open(email);
  }

  sendCheckoutData(total) {
    let navigationExtras: NavigationExtras = {
      state: {
        totalCheckout: total,
      }
    };
    this.router.navigate(['client/checkout'], navigationExtras)
  }


}
