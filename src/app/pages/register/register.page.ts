import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  registerForm: FormGroup;

  constructor(private router: Router, private authService: AuthenticationService, public formBuilder: FormBuilder) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: [''],
      email: [''],
      password: [''],
      address: [''],
      admin: []
    })
  }

  onSubmit(){
    if (!this.registerForm.valid) {
      return false;
    } else {
      this.signUp(this.registerForm.value.username, 
        this.registerForm.value.email, 
        this.registerForm.value.password, 
        this.registerForm.value.address,
        this.registerForm.value.admin)
    }
  }

  signUp(username, email, password, address, admin){
    this.authService.registerUser(username, email, password, address, admin)     
    .then((res) => {
      this.authService.signIn(email, password);
      
      admin == null ? admin = false : admin = true;
      localStorage.setItem('admin', admin)
      localStorage.setItem('authenticated', 'true');
      
      if (admin) {
        this.router.navigate(['admin/home']);
      } else {
        this.router.navigate(['client/home']);
      }
    }).catch((error) => {
      window.alert(error.message)
    })
  }
 

}
