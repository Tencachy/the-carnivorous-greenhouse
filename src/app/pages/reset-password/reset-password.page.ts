import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.page.html',
  styleUrls: ['./reset-password.page.scss'],
})
export class ResetPasswordPage implements OnInit {

  resetPasswordForm: FormGroup
  user: any

  constructor(private router: Router, private authService: AuthenticationService, public formBuilder: FormBuilder) { }

  ngOnInit() {
    this.resetPasswordForm = this.formBuilder.group({
      email: ['']
    })
  }

  onSubmit() {
    if (!this.resetPasswordForm.valid) {
      return false;
    } else {
      this.sendResetPasswordEmail(this.resetPasswordForm.value.email)
    }
  }

  sendResetPasswordEmail(email) {
    console.log(email)
    this.authService.resetPassword(email).then((res => {
      console.log('THEN')
      this.router.navigate(['login'])
    }))
  }

}
