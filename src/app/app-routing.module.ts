import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AdminGuard } from './guards/admin.guard';
import { AuthGuard } from './guards/auth.guard';
import { LoginGuard } from './guards/login.guard';
import { ClientGuard } from './guards/client.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'launch',
    pathMatch: 'full'
  },
  {
    path: 'launch',
    loadChildren: () => import('./pages/launch/launch.module').then( m => m.LaunchPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule),
    canLoad: [LoginGuard]
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule),
    canLoad: [LoginGuard]
  },
  {
    path: 'admin/home',
    loadChildren: () => import('./pages/admin/home/home.module').then( m => m.HomePageModule),
    canLoad: [AuthGuard, AdminGuard]
  },
  {
    path: 'admin/product-list',
    loadChildren: () => import('./pages/admin/product-list/product-list.module').then( m => m.ProductListPageModule),
    canLoad: [AuthGuard, AdminGuard]
  },
  {
    path: 'admin/product-create',
    loadChildren: () => import('./pages/admin/product-create/product-create.module').then( m => m.ProductCreatePageModule),
    canLoad: [AuthGuard, AdminGuard]
  },
  {
    path: 'admin/product-edit/:id',
    loadChildren: () => import('./pages/admin/product-edit/product-edit.module').then( m => m.ProductEditPageModule),
    canLoad: [AuthGuard, AdminGuard]
  },
  {
    path: 'admin/client-list',
    loadChildren: () => import('./pages/admin/client-list/client-list.module').then( m => m.ClientListPageModule),
    canLoad: [AuthGuard, AdminGuard]
  },
  {
    path: 'client/home',
    loadChildren: () => import('./pages/client/home/home.module').then( m => m.HomePageModule),
    canLoad: [AuthGuard, ClientGuard]
  },
  {
    path: 'admin/map',
    loadChildren: () => import('./pages/admin/map/map.module').then( m => m.MapPageModule),
    canLoad: [AuthGuard, AdminGuard]
  },
  {
    path: 'client/checkout',
    loadChildren: () => import('./pages/client/checkout/checkout.module').then( m => m.CheckoutPageModule),
    canLoad: [AuthGuard, ClientGuard]
  },
  {
    path: 'reset-password',
    loadChildren: () => import('./pages/reset-password/reset-password.module').then( m => m.ResetPasswordPageModule),
    canLoad: [LoginGuard]
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
