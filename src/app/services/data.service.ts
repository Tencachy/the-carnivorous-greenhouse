import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Address, Product } from '../interfaces/interface';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
const MAPTOKEN = environment.mapBoxToken
const URL = 'https://api.mapbox.com/geocoding/v5/mapbox.places/';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private firestore: AngularFirestore, private router: Router, private http: HttpClient) { }

  getProducts() {
    return this.firestore.collection('Products').snapshotChanges();
  }

  getProduct(id) {
    return this.firestore.collection('Products').doc(id).valueChanges();
  } 

  deleteProduct(id) {
    this.firestore.doc('Products/'+id).delete();
  }
 
  createProduct(product: Product){
    return this.firestore.collection('Products').add(product);
  }
 
  updateBook(id, product: Product) {
    this.firestore.collection('Products').doc(id).update(product)
      .then(() => {
        this.router.navigate(['admin/product-list']);
      }).catch(error => console.log(error));
  }

  getClients() {
    return this.firestore.collection('users').snapshotChanges();
  }

  getClient(id) {
    return this.firestore.collection('users').doc(id).valueChanges();
  }

  getCoordinates(address) {
    return this.http.get<Address>(URL + address + '.json?access_token=' + MAPTOKEN)
  }
}
