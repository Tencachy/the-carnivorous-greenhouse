import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import { User } from '../interfaces/interface';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  userData: any;

  constructor(public afStore: AngularFirestore,
    public ngFireAuth: AngularFireAuth, public router: Router) {
    this.ngFireAuth.authState.subscribe(user => {
      if (user) {
        this.userData = user;
        console.log('USER: ', this.userData)
        localStorage.setItem('user', JSON.stringify(this.userData));
      } else {
        localStorage.setItem('user', null);
      }
    })
  }

  signIn(email, password) {
    return this.ngFireAuth.signInWithEmailAndPassword(email, password)
  }
  
  registerUser(username, email, password, address, admin) {
    return this.ngFireAuth.createUserWithEmailAndPassword(email, password)
    .then((result) => {
      const userData: User = {
        uid: email,
        email: email,
        displayName: username,
        address: address,
        admin: admin}
      this.setUserData(userData);
    });
  }

  setUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.afStore.doc(`users/${user.uid}`);
    if (user.admin == null) user.admin = false
    const userData: User = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      address: user.address,
      admin: user.admin,
    }
    return userRef.set(userData)
  } 

  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return (user !== null) ? true : false;
  }

  signOut() {
    return this.ngFireAuth.signOut().then(() => {
      localStorage.removeItem('user');
      localStorage.removeItem('authenticated');
      localStorage.removeItem('admin');
      this.router.navigate(['login']);
    })
  } 

  getUser(id) {
    return this.afStore.collection('users').doc(id).valueChanges();
  } 

  resetPassword(email) {
    return this.ngFireAuth.sendPasswordResetEmail(email);
  }

}
